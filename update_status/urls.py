from django.conf.urls import url
from .views import index, add_status, remove_status

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^remove_status', remove_status, name='remove_status'),
]

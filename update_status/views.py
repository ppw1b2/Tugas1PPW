from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Nathanael Lemuella" #TODO Implement yourname
    status = Status.objects.all()
    response['status'] = status
    html = 'update_status/update_status.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/update_status/')
    else:
        return HttpResponseRedirect('/update_status/')

def remove_status(request):
    try:
        idObjek = request.POST['flag']
        Status.objects.filter(id=idObjek).delete()
    except ValueError or KeyError:
        pass
    return HttpResponseRedirect('/update_status/')


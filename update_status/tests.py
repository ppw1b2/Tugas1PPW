from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, remove_status
from .models import Status
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# # Create your tests here.
# class UpdateStatusFunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome(
#             './chromedriver', chrome_options=chrome_options)
#         super(UpdateStatusFunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(UpdateStatusFunctionalTest, self).tearDown()

#     def test_input_todo(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/update_status/')
#         # find the form element
#         description = selenium.find_element_by_id('id_description')
#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         description.send_keys(
#             'Tugas kali ini menggunakan Selenium untuk Test nya')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)


class UpdateStatusUnitTest(TestCase):
    def test_update_status_url_is_exist(self):
        response = Client().get('/update_status/')
        self.assertEqual(response.status_code, 200)

    def test_update_status_using_index_func(self):
        found = resolve('/update_status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(
            description='mengerjakan tugas 1 ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_update_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(
            '/update_status/add_status', {'description': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/update_status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_update_status_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(
            '/update_status/add_status', {'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('update_status')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_update_status_delete_post(self):
        new_status = Status.objects.create(description = "internet")
        object = Status.objects.all()[0]
        response_post = Client().post('/update_status/remove_status', {'flag': str(object.id)})

        response= Client().get('/update_status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<form id="form-' + str(object.id) + '"', html_response)
		
    def test_update_status_error_flag(self):
        new_status = Status.objects.create(description = "Apapaun")
        object = Status.objects.all()[0]
        response_post = Client().post('/update_status/remove_status', {'flag': "Delete!!"})

        response= Client().get('/update_status/')
        html_response = response.content.decode('utf8')
        self.assertIn('<form id="form-' + str(object.id) + '"', html_response)


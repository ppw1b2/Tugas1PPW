from django.db import models

# Create your models here.
class Profile(models.Model):
	name = models.CharField(max_length=32)
	birth_date = models.CharField(max_length=6)
	gender = models.CharField(max_length=6)
	expertise = models.TextField(max_length=150)
	description = models.CharField(max_length=200)
	email = models.CharField(max_length=100)
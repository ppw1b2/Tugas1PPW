from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Profile

# Create your tests here.

class Halaman_ProfileUnitTest(TestCase):
	def test_halaman_profile_url_is_exist(self):
		response = Client().get('/halaman_profile/')
		self.assertEqual(response.status_code, 200)

	def test_halaman_profile_using_index_func(self):
		found = resolve('/halaman_profile/')
		self.assertEqual(found.func,index)
	
	def test_profile(self):
		nama = "Nael"
		birthday = "23 Mar"
		gender = "male"
		expertise = ["Programmer","Copy","Paste"]
		description = "coba dulu aja"
		email = "nael@hmail.com"
		profile = Profile(name=nama, birth_date=birthday, gender=gender, description=description, email=email, expertise=expertise)
		self.assertEqual(profile.name,nama)
		self.assertEqual(profile.birth_date,birthday)
		self.assertEqual(profile.gender,gender)
		self.assertEqual(profile.description,description)
		self.assertEqual(profile.expertise,expertise)
		self.assertEqual(profile.email,email)
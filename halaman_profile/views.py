from django.shortcuts import render
from .models import Profile

# Create your views here.
name ="Kelompok 02"
birth_date = "03 Okt"
gender = "Male"
description = "Impossible it's Nothing. Your Future Programmer"
email = "Kelompok02PPW@gmail.com"
expertise = ["Programmer","Hardworker","Students"]

Profile = Profile(name=name, birth_date=birth_date, gender=gender, description=description, email=email, expertise=expertise)

def index(request):
	response = {'Nama': Profile.name,'Birthday': Profile.birth_date, 'Gender': Profile.gender, 'Expertise': Profile.expertise, 'Description': Profile.description, 'Email': Profile.email}
	html = 'halaman_profile/halaman_profile.html'
	return render(request,html,response)
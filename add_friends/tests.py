from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friends
from .models import Post
from .forms import Todo_Form

# Create your tests here.
class AddFriendsUnitTest(TestCase):

    def test_add_friends_url_is_exist(self):
        response = Client().get('/add_friends/')
        self.assertEqual(response.status_code, 200)

    def test_add_friends_using_index_func(self):
        found = resolve('/add_friends/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_post(self):
        #Creating a new post
        new_post = Post.objects.create(name='test',heroku_url='This is a test')

        #Retrieving all available post
        counting_all_available_post= Post.objects.all().count()
        self.assertEqual(counting_all_available_post,1)

    def test_page_friends_post_success_and_render_the_result(self):
        response_post = Client().post('/add_friends/add_friends', {'name': 'dummy', 'heroku_url': 'http://dummy.herokuapp.com'})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/add_friends/')
        html_response = response.content.decode('utf8')
        self.assertIn('dummy',html_response)
        self.assertIn('http://dummy.herokuapp.com',html_response)

    def test_page_friends_post_fail(self):
        response = Client().post('/add_friends/add_friends', {'name': 'Anonymous', 'heroku_url': 'netnot'})
        self.assertEqual(response.status_code, 302)

    

    



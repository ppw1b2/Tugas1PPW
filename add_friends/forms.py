from django import forms

class Todo_Form(forms.Form):
    error_messages = {
    	'required': 'Tolong isi input ini',
    	'invalid': 'Isi input dengan url'
    }
    name_attrs = {
        'type': 'text',
        'class': 'todo-form-name',
        'placeholder':'Masukkan nama...'
    }
    heroku_url_attrs = {
        'type': 'text',
        'class': 'todo-form-heroku_url',
        'placeholder':'Masukkan heroku url...'
    }
    name = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    heroku_url = forms.URLField(label='', required=True, max_length=200, widget=forms.TextInput(attrs=heroku_url_attrs))
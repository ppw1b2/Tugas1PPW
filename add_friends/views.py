from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Post

# Create your views here.
response = {'author': "Kelompok 2 - PPW B"}

def index(request):
    response['Todo_form'] = Todo_Form
    post = Post.objects.all()
    response['post'] = post
    html = 'add_friends/add_friends.html'
    return render(request, html, response)

def add_friends(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['heroku_url'] = request.POST['heroku_url']
        post = Post(name=response['name'],heroku_url=response['heroku_url'])
        post.save()
        return HttpResponseRedirect('/add_friends/')
    else :
        return HttpResponseRedirect('/add_friends/')

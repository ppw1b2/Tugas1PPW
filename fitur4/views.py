from django.shortcuts import render
from update_status.models import Status
from add_friends.models import Post
from halaman_profile.models import Profile


# Create your views here.
def index(request):
	totalstatus = Status.objects.count();
	totalfriend = Post.objects.count();
	#name = Profile.object.name();
	lastpost = Status.objects.last();

	response = {'name' : "Kelompok 02", 'totalstatus':totalstatus, 'totalfriend':totalfriend, 'lastpost':lastpost}
	return render(request,'fitur4/stathtml.html',response)

from django.test import TestCase
from django.urls import resolve
from django.test import Client
from .views import index
from update_status.models import Status
from add_friends.models import Post
from django.utils import timezone
from django.http import HttpRequest

# Create your tests here.
class StatisticTest(TestCase):

	def test_statistic_url_is_exist(self):
		response = Client().get('/fitur4/')
		self.assertEqual(response.status_code, 200)
		
	def test_statistic_using_index_func(self):
		found = resolve('/fitur4/')
		self.assertEqual(found.func, index)

	def testing_nav_and_footer(self):
		request = HttpRequest()
		response = Client().get('/fitur4/')
		html_response = response.content.decode('utf8')
		self.assertIn('navbar',html_response)
		self.assertIn('Made by',html_response)

	def testing_status_amount(self):
		status_test = 'Coba coba'
		
